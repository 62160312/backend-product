const express = require('express')
const router = express.Router()
const Product = require('../models/Product')
// const products = [
//   { id: 1, name: 'IPad gen1 64G wifi', price: 11000.0 },
//   { id: 2, name: 'IPad gen2 64G wifi', price: 12000.0 },
//   { id: 3, name: 'IPad gen3 64G wifi', price: 13000.0 },
//   { id: 4, name: 'IPad gen4 64G wifi', price: 14000.0 },
//   { id: 5, name: 'IPad gen5 64G wifi', price: 15000.0 },
//   { id: 6, name: 'IPad gen6 64G wifi', price: 16000.0 },
//   { id: 7, name: 'IPad gen7 64G wifi', price: 17000.0 },
//   { id: 8, name: 'IPad gen8 64G wifi', price: 18000.0 },
//   { id: 9, name: 'IPad gen9 64G wifi', price: 19000.0 },
//   { id: 10, name: 'IPad gen10 64G wifi', price: 20000.0 }
// ]
// const lastID = 11

const getProducts = async function (req, res, next) {
  try {
    const products = await Product.find({}).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getProduct = async function (req, res, next) {
  const id = req.params.id

  try {
    const product = await Product.findById(id).exec()
    if (product === null) {
      return res.status(404).json({
        message: 'Product not found!!!'
      })
    }
    res.json(product)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addProducts = async function (req, res, next) {
  // console.log(req.body)
  // const newProduct = {
  //   id: lastID,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // products.push(newProduct)
  // lastID++
  // res.status(201).json(newProduct)
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}
const updateProduct = async function (req, res, next) {
  const productId = req.params.id
  try {
    const product = await Product.findById(productId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    return res.status(200).json(product)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }

  // const product = {
  //   id: productID,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // const index = products.findIndex(function (item) {
  //   return item.id === productID
  // })
  // if (index >= 0) {
  //   products[index] = product
  //   res.json(products[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No product id ' + req.params.id
  //   })
  // }
}
const deleteProduct = async function (req, res, next) {
  const productId = req.params.id
  try {
    await Product.findByIdAndDelete(productId)
    res.status(204).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
  // const index = products.findIndex(function (item) {
  //   console.log(productid)
  //   return item.id === productid
  // })
  // if (index >= 0) {
  //   products.splice(index, 1)
  //   res.status(204).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No product id ' + req.params.id
  //   })
  // }
}
router.get('/', getProducts)
router.get('/:id', getProduct)
router.post('/', addProducts)
router.put('/:id', updateProduct)
router.delete('/:id', deleteProduct)

module.exports = router
